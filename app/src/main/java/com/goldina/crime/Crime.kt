package com.goldina.crime

data class Crime(
    var title: String = "Непридумышленное убийство",
    var id: Int = 1,
    var date: String = "25-03-2022 15:00",
    var isSolved: Boolean =false,
)