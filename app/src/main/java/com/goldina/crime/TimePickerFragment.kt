package com.goldina.crime

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import java.util.*


@Suppress("DEPRECATION")
class TimePickerFragment : DialogFragment() {
    private lateinit var mTimePicker: TimePicker
    private lateinit var buttonTime:Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_time_picker, container, false)
        mTimePicker = view.findViewById(R.id.timePicker)
        buttonTime =view.findViewById(R.id.button)

        val now: Calendar = Calendar.getInstance()
        mTimePicker.currentHour = now.get(Calendar.HOUR_OF_DAY)
        mTimePicker.currentMinute = now.get(Calendar.MINUTE)
        buttonTime.setOnClickListener {
            val time = "${mTimePicker.currentHour}:${mTimePicker.currentMinute}"
            val intent= Intent()
            intent.putExtra("time", time)
            targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
            dismiss()
        }
        return view
    }

}