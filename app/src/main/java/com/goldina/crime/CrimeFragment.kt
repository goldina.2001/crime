package com.goldina.crime

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*


@Suppress("DEPRECATION")
class CrimeFragment : Fragment() {
    private lateinit var buttonDate:Button
    private lateinit var buttonTime:Button
    private lateinit var editTextTitle: EditText
    private lateinit var editTextID: EditText
    private lateinit var textViewDate:TextView
    private lateinit var checkBox: CheckBox
    private val REQUEST_CODE = 1
    private val REQUEST_CODE_ANOTHER = 2
    val crime = Crime()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
         val view =  inflater.inflate(R.layout.fragment_crime, container, false)
        buttonDate = view.findViewById(R.id.buttonDate)
        buttonTime= view.findViewById(R.id.buttonTime)
        editTextTitle = view.findViewById(R.id.editTextTitle)
        editTextID = view.findViewById(R.id.editTextID)
        textViewDate = view.findViewById(R.id.textViewDate)
        checkBox = view.findViewById(R.id.checkBoxSolved)
        editTextTitle.setText(crime.title)
        editTextID.setText(crime.id.toString())
        textViewDate.text = crime.date
        checkBox.isChecked = crime.isSolved

        buttonDate.setOnClickListener {
            val dialog = DataPickerFragment()
            dialog.setTargetFragment(this, REQUEST_CODE)
            dialog.show(parentFragmentManager,"dialogDate")
        }
        buttonTime.setOnClickListener {
            val dialog = TimePickerFragment()
            dialog.setTargetFragment(this, REQUEST_CODE_ANOTHER)
            dialog.show(parentFragmentManager,"dialogDate")
        }
        checkBox.setOnCheckedChangeListener { _, b ->
            if(b){
                Toast.makeText(activity, "Сохранено", Toast.LENGTH_SHORT).show()
                crime.title=editTextTitle.text.toString()
                crime.id = editTextID.text.toString().toInt()
                crime.date=textViewDate.text.toString()
                crime.isSolved=true
            }
        }
        return view
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            REQUEST_CODE->{
                val date = data?.getStringExtra("date").toString()
                val rg= Regex("\\d{1,2}-\\d{1,2}-\\d{4}")
                crime.date = rg.replace(crime.date,date)
            }
            REQUEST_CODE_ANOTHER->{
                val time = data?.getStringExtra("time").toString()
                val rg= Regex("\\d{1,2}:\\d{1,2}")
                crime.date = rg.replace(crime.date,time)
            }
        }
        textViewDate.text = crime.date
    }

}