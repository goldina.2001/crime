package com.goldina.crime

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import java.util.*


@Suppress("DEPRECATION")
class DataPickerFragment : DialogFragment() {
    private lateinit var mDatePicker: DatePicker
    private lateinit var changingDateButton:Button

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_date_picker, container, false)
        mDatePicker = view.findViewById(R.id.datePicker)

        changingDateButton = view.findViewById(R.id.button)

        val today = Calendar.getInstance()

        mDatePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)
        ) { _, _, _, _ ->        }
        changingDateButton.setOnClickListener {
            val date = "${mDatePicker.dayOfMonth}-${mDatePicker.month + 1}-${mDatePicker.year}"
            val intent=Intent()
            intent.putExtra("date", date)
            targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
            dismiss()
        }
        // Inflate the layout for this fragment
        return view
    }


}